# Python has several structures to store collections or multiple items in a single variable, namely:
# List [], dictionary, tuples (), set {}
	# Tuples - ("Math", "Science", "English")
	# Set - {"Math", "Science", "English"}

# [Section] Lists
# Lists are similar to JS arrays in the sense that they can contain a collection of data.
# To create a list, use square brackets [].

names = ["John", "George", "Ringo"] # An example of a string list.
programs = ["developer career", "pi-shape", "short courses"]
# durations = [260, 180, 20] # Number list
truth_variables = [True, False, True, True, False] # Boolean list

# A list can contain elements with different data types:
sample_list = ["Apple", 3, False, "Potato", 4, False]

# Problems may arise if you try to create lists that contain multiple data types; it's best to use dictionaries for that.

# Getting the list size
# Use the len() method

print(len(programs))

var_len = len(sample_list)

print(var_len)

# Accessing values/elements in a list
# The contents of a list can be accessed by providing the index number of the element.
# They can also be accessed using the negative index.

print(names[0])

print(names[-1]) # We can use -1 to access the last item in the list without knowing its index number, since -1 always represents the last item in the list.

print(programs[1])

#To access the whole list, simply enter the list's name.
print(programs)

# Access a range of values
print(programs[0:2]) # This returns "developer career" and "pi-shape", as the code stops before "short courses", whose index number is 2.

# Updating lists
print(f"Current value: {programs[2]}")

# Update the value
programs[2] = "ShortCourses"
print(f"New value: {programs[2]}")

# [Section] List Manipulation
# There are methods that can be used to manipulate the elements within a list.

# Adding a list item - append()

programs.append("global")
print(programs)

# Deleting a list item - "del" keyword
durations = [260, 180, 20]
durations.append(360)
print(durations)

del durations[-1]
print(durations)

del durations[0]
print(durations)

# insert() method; list_name.insert(index_to_put_item_in, item_to_be_added)
durations.insert(0, 100)
print(durations)

durations.insert(1, 99)
print(durations)

# Membership checking - "in" keyword; same as includes() in JS, it checks if the element is in the list and returns True or False

print(20 in durations) # Returns True
print(500 in durations) # Returns False

# Sorting lists - sort() arranges the list's contents alphabetically by default

durations.sort()
print(durations)

programs.sort()
print(programs)

# Emptying the list - clear()
test_list = [1, 3, 5, 7, 9]
print(test_list)

test_list.clear()
print(test_list)



# [Section] Dictionaries are used to store data values in key:value pairs, similar to JS objects.
# A dictionary is a collection of data which is ordered, changeable, and does not allow duplicate keys and values.
# To create a dictionary, use curly braces {} and set key: value pairs inside.

person1 = {
	"name" : "Brandon",
	"age" : 28,
	"occupation" : "student",
	"isEnrolled" : True,
	"subjects" : ["Python", "SQL", "Django"]
}

# To get the number of key-value pairs on the dictionary, one can use the len() method.
print(len(person1))

# Accessing values in a dictionary
# Unlike JS, dot notation will not work in Python. The key name must be referred to using square brackets.
print(person1["name"])
# print(person1.name)

# The keys() method will return the keys of a dictionary
print(person1.keys())

# Meanwhile, the values() method will return a list of all the values in the dictionary.
print(person1.values())

# The items() method will return each item in a dictionary as a key-value pair in a list.
print(person1)
print(person1.items())

# Adding key-value pairs can be done with either a new index key and assigning a value or the update method.
# index key
person1["nationality"] = "Filipino"
print(person1)

# update() method
person1.update({"fav_food" : "Sinigang"})
print(person1)

# Deleting entries - done by pop() method and the del keyword
# pop() method
person1.pop("fav_food")
print(person1)

# using del keyword
del person1["nationality"]
print(person1)

# clear() empties the dictionary.
person2 = {
	"name" : "John",
	"age" : 18
}

# person2.clear()
print(person2)

# Looping through a dictionary
for prop in person1:
	print(f"The value of {prop} is {person1[prop]}") # {prop} represents the elements in person1. Because of the bracket notation, {person1[prop]} will return the value associated with the key represented by the identifier {prop}.
	# prop represents the key in a key: value pair, while person1[prop] represents the value that would correspond to the key in the dictionary.

# Nested dictionaries - dictionaries can be nested inside another dictionary.
person3 = {
	"name" : "Monika",
	"age" : 20,
	"occupation" : "poet",
	"isEnrolled" : True,
	"subjects" : ["Python", "SQL", "Django"]
}

print(person3["subjects"][0])
print(person3["subjects"][-1])

class_room = {
	"student1" : person1,
	"student2" : person3
}

print(class_room["student2"]["subjects"][1])

# [Section] Functions
# Functions are blocks of code that run when called.
# A function can be used to get inputs, process inputs, and return inputs.
# def : Python :: function : JS
# Syntax:
	# def <function_name>(<parameter(s)>) :

# Define a function called my_greeting
def my_greeting() :
	print("Hello, user!")

# Calling or invoking a function: To use a function, just specify the function name and provide the value or values needed, if any.
my_greeting()

# Parameters can be added to functions so that the user can enter their input.

def greet_user(username) :
	print(f"Hello, {username}!")

# Arguments are the values submitted to the parameters.
greet_user("Gil")

# Return statements: The return keyword allows functions to return values.

def addition(x, y) :
	return x + y

sum = addition(5, 9)
print(f"The sum is {sum}.")

# [Section] Lambda Functions
# A lambda function is an anonymous or unnamed functions with implicit return; can be used for callbacks
# It is just like any normal Python function, except that it has no name when it is defined, and it is contained in one line of code.
# A lambda function can take any number of arguments, but can only have one expression.

greeting = lambda person : f"Hello, {person}!"

greeting("Elsie") # This won't return anything, since the function doesn't have an innate print().

print(greeting("Elsie"))

mult = lambda a, b : a * b

print(mult(5, 6))

# [Section] Classes
# Classes would serve as blueprints to describe the concept of objects.
# Each object has characteristics (properties or key-value pairs) and behaviors (methods).

# To create a class, use the "class" keyword followed by the desired class name (started with an uppercase character).

class Car():
	# Properties that all car objects must have are defined in a method called "init".

	# This line initializes the properties of our function. "self" is always placed before any other desired properties.
	def __init__(self, brand, model, year_of_make) :
		self.brand = brand
		self.model = model
		self.year_of_make = year_of_make

		# Properties that are hard-coded and not defined by user input
		self.fuel = "Gasoline"
		self.fuel_level = 0

	# Method (don't forget to add "self" as well in the parameters)
	def fill_fuel(self) :
		print(f"Current fuel level: {self.fuel_level}")
		print("Filling up the fuel tank. . .")
		self.fuel_level = 100
		print(f"New fuel level: {self.fuel_level}")

	def drive(self, distance) :
		print(f"The car has driven {distance}km.")

		self.fuel_level = self.fuel_level - distance # self.fuel_level -= distance also works.
		print(f"Current fuel level: {self.fuel_level}")

# Instantiate a class
new_car = Car("Nissan", "GT-R", 2019)

print(new_car.brand)
print(new_car.model)
print(new_car.year_of_make)

print(new_car)

# To invoke a class' method
new_car.fill_fuel()
print(new_car.fuel_level)

new_car.drive(50)